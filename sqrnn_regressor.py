import numpy as np
import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras import Model, Input
from tensorflow.keras.layers import Activation, Dense, BatchNormalization, Dropout, Lambda, Concatenate
from tensorflow.keras.callbacks import Callback
from sklearn.metrics import mean_absolute_error


def set_seed(seed):
    import os
    import random
    os.environ['PYTHONHASHSEED'] = str(seed)
    if tf.__version__ in ['2.0.0', '2.1.0']:
        os.environ['TF_CUDNN_DETERMINISTIC'] = '1'
        tf.random.set_seed(seed)
    else:
        tf.set_random_seed(seed)
    random.seed(seed)
    np.random.seed(seed)


class SQRNNRegressor:
    def __init__(self, hidden=[32], epochs=15, optimizer='adam', dropout=.25):
        self.hidden = hidden
        self.epochs = epochs
        self.optimizer = optimizer
        self.dropout = dropout
        self.nn_batch_size = 128
        self.n_input = None
        self.validation_split = 0.2

    def skip_connection_network_2(self, dim, std_y):
        def get_slice(var=None):
            if var == 'tau':
                def func(x):
                    return x[:, -1:]
            else:
                def func(x):
                    return x[:, :-1]
            return Lambda(func)

        def placeholder_loss(y, f, q=.5):
            if tf.__version__ in ['2.0.0', '2.1.0']:
                return y * 0.
            else:
                return K.mean(K.maximum(q * (f - y), (q - 1) * (f - y)), axis=-1)

        self.n_input = dim + 1
        inputs = Input(shape=[self.n_input])
        tau = get_slice('tau')(inputs)
        x = Dense(self.hidden[0])(get_slice()(inputs))
        x = Activation("relu")(x)
        x = BatchNormalization()(x)
        x = Dropout(self.dropout)(x)
        x = Concatenate()([x, tau])
        for hidden in self.hidden[1:]:
            x = Dense(hidden)(x)
            x = Activation("relu")(x)
            x = BatchNormalization()(x)
            x = Dropout(self.dropout)(x)
        out = Dense(1)(x)
        model = Model(inputs=inputs, outputs=out, name="sqrnn_regressor")
        model.compile(loss=placeholder_loss, optimizer=self.optimizer)
        layer_nr_of_dense_after_concat = [layer.name for layer in model.layers].index("concatenate") + 1
        weight = model.layers[layer_nr_of_dense_after_concat].get_weights()[0]
        bias = model.layers[layer_nr_of_dense_after_concat].get_weights()[1]
        weight[-1] = [std_y]
        model.layers[layer_nr_of_dense_after_concat].set_weights([weight, bias])
        return model

    def augment(self, X, tau=None):
        if tau is None:
            tau = .5
        tau = np.zeros((X.shape[0], 1)) + tau
        tau = tau - 0.5
        return np.concatenate((X, tau), axis=1)

    def drop_remainder(self, X, y):
        samples_used = (X.shape[0] // self.nn_batch_size) * self.nn_batch_size
        valid_nn = (samples_used * self.validation_split // self.nn_batch_size) * self.nn_batch_size
        return X[:samples_used, :], y[:samples_used], valid_nn / samples_used

    def fit(self, X, y):
        self.model = self.skip_connection_network_2(dim=X.shape[1], std_y=np.std(y))
        if tf.__version__ in ['2.0.0', '2.1.0']:
            X, y, self.validation_split = self.drop_remainder(X, y)

        class SQRNNLoss(Callback):
            def __init__(self, total_q):
                self.total_q = total_q

            def on_batch_begin(self, batch, logs=None):
                def tilted_loss(y, f, q, sample_weights=None):
                    #diff = y - f
                    #mask = tf.stop_gradient(tf.compat.v1.to_float(K.less(0. * diff, diff)) - q)
                    #return K.mean((mask * diff), axis=-1)
                    return K.mean(K.maximum(q * (f - y), (q - 1) * (f - y)), axis=-1)

                if batch == 0:
                    self.batch_size = logs['size']
                batch_start_idx = batch * self.batch_size
                q_ = self.total_q[batch_start_idx:(batch_start_idx + logs['size'])]
                self.model.loss_functions[0] = lambda y, f, sample_weight: tilted_loss(y, f, q_, sample_weight)

        q = np.random.rand(y.shape[0], 1)
        self.model.fit(self.augment(X, tau=q), y, epochs=self.epochs, batch_size=self.nn_batch_size,
                       verbose=2, callbacks=[SQRNNLoss(q)], validation_split=self.validation_split)

    def iqr_cover(self, X, y, cover=.5):
        y_lower = self.predict(X, q=.5 - (cover / 2.))
        y_upper = self.predict(X, q=.5 + (cover / 2.))
        return sum((y >= y_lower) & (y <= y_upper)) / len(y)

    def show_results(self, X, y, cover=.5):
        print(f'Show results for IQR-Cover: {(cover * 100.):.2f}%')
        print(f'MAE = {mean_absolute_error(y, self.predict(X)):.2f}')
        print(f'IQR = {np.mean(self.iqr(X, cover)):.2f}')
        cover_pred = self.iqr_cover(X, y, cover)
        print(f'IQR covers {(cover_pred * 100.):.2f}% of true values.')
        return cover_pred

    def iqr(self, X, cover):
        return self.predict(X, q=.5 + cover / 2.) - self.predict(X, q=.5 - cover / 2.)

    def predict(self, X, q=.5):
        tau = np.zeros((X.shape[0], 1)) + q
        y_pred = self.model.predict(self.augment(X, tau), batch_size=16)
        return np.squeeze(y_pred)
