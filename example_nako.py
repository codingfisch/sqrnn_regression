import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'    # No TF-Logging
os.environ['TF_ENABLE_AUTO_MIXED_PRECISION'] = '1'  # Use Mixed-Precision (FP16 and FP32)
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import VarianceThreshold
from tensorflow.keras.optimizers import Adam
from sqrnn_regressor import set_seed, SQRNNRegressor

seed = 42
set_seed(seed)
train_sample_nn = 5000

local = 'lukas'

if local == 'tim':
    f = r'C:\Users\Tim\Google Drive\work\all_skripts\py_code\NAKO_BrainAge/'
elif local == 'lukas':
    f = '/home/lukas/Daten/Nako/'
else:
    f = '/spm-data/Scratch/spielwiese_tim/BrainAge/'
    f_y = '/spm-data/Scratch/spielwiese_vincent/NAKO/'

X = np.load(f + 'X_3mm.npy')
X = StandardScaler().fit_transform(X)
X = VarianceThreshold().fit_transform(X)
if local not in ['tim', 'lukas']:
    data = pd.read_csv(f_y + 'NAKO_data.csv')
else:
    data = pd.read_csv(f + 'NAKO_data.csv')
data = data.dropna(subset=['df100_age', 'GM_path'])
data = data.reset_index()
y = data['df100_age'].to_numpy().astype(float)
#X, y = X[:train_sample_nn, :], y[:train_sample_nn]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=seed)

regressor = SQRNNRegressor(hidden=[32], epochs=15, optimizer=Adam(learning_rate=.003))
regressor.fit(X_train, y_train)

covers = [.5, .7, .9]
covers_pred = []
for c in covers:
    covers_pred.append(regressor.show_results(X_test, y_test, cover=c))
c_delta = sum([abs(c - c_pred) for c, c_pred in zip(covers, covers_pred)])
print(f'Average cover delta: {c_delta / len(covers) * 100.:.2f}%')
